# Installation and Usage 

    install.packages("devtools")
    
    library(devtools)
    install_bitbucket("cbio_mskcc/pancanmet",
        build_vignette=TRUE,
        dependencies=TRUE,
        args="--no-multiarch")
        
    library(pancanmet)
    runShinyApp()