### About the Data Explorer
Built by Augustin Luna, B. Arman Aksoy, and Ed Reznik using R Shiny.

### Data Attribution
We are extremely grateful to the data providers who have made this resource possible. Attribution for the metabolomic data is listed below. All gene expression data is obtained from The Cancer Genome Atlas Project.

* *BLCA*: Putluri et al. "Metabolomic profiling reveals potential markers and bioprocesses altered in bladder cancer progression." Cancer Res. 2011.

* *BRCA*: Terunama et al. "MYC-driven accumulation of 2-hydroxyglutarate is associated with breast cancer prognosis." J Clin Invest. 2014.

* *BRCATang*: Tang et al. "A joint analysis of metabolomics and genetics of breast cancer." Breast Cancer Res. 2014.

* *KIRC*: Hakimi et al. "An Integrative Metabolic Atlas of Clear Cell Renal Cell Carcinoma." Unpublished.

* *LGG*: Chinnaiyan et al. "The metabolomic signature of malignant glioma reflects accelerated anabolic metabolism." Cancer Res. 2012.

* *OV*: Fong et al. "Identification of Metabolites in the Normal Ovary and Their Transformation in Primary and Metastatic Ovarian Cancer." PLoS One. 2011.

* *PAAD*: Kamphorst et al. "Human pancreatic cancer tumors are nutrient poor and tumor cells actively scavenge extracellular protein." Cancer Res. 2015.

* *PAADHussain1/PAADHussain2*: Zhang et al. "Integration of Metabolomics and Transcriptomics Revealed a Fatty Acid Network Exerting Growth Inhibitory Effects in Human Pancreatic Cancer." Clinical Cancer Res. 2013.

* *PRAD*: Sreekumar et al. "Metabolomic profiles delineate potential role for sarcosine in prostate cancer progression." Nature 2009.

* *PRADLODA*: Priolo et al. "AKT1 and MYC Induce Distinctive Metabolic Fingerprints in Human Prostate Cancer." Cancer Res. 2014.

### Feedback
This web application is still under heavy development, and we appreciate any feedback/suggestions you may have. Please forward feedback to reznike@mskcc.org.
