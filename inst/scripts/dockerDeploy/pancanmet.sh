#!/bin/bash

echo "Pulling new version"
sudo docker pull cannin/pancanmet

echo "Stopping new container"
sudo docker stop pancanmet
sudo docker rm pancanmet

echo "Starting new container"
sudo docker run --restart always --name pancanmet -d -p 3840:3838 -t cannin/pancanmet shiny-server
