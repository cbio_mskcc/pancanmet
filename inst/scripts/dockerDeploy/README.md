# Deployment with CaptainHook
* http://blog.gopheracademy.com/advent-2014/easy-deployment/
* https://github.com/ArturoBlas/docker-captainhook

# Pull captainhook Docker image
sudo docker pull cannin/captainhook

# Run captainhook Docker image
sudo docker run -d -p 8080:8080 -v /webhooks:/webhooks cannin/captainhook

# Run from local installation
* https://github.com/bketelsen/captainhook
captainhook -listen-addr=0.0.0.0:3830 -echo -configdir /webhooks &

# Trigger captainhook
* NOTE: Must run: chmod +x kidneyMetabProject.sh (like 775 permissions for the .sh)
curl -X POST http://127.0.0.1:3830/kidneyMetabProject

# Debug Shiny
# Command
    sudo docker rm -f shinydebug; sudo docker run -i --restart always --name shinydebug -p 3900:3838 -t cannin/emdemo bash

# Rename Project Folder
Rename project folder /srv/shiny-server/NAME to /srv/shiny-server/shinydebug
    mv /srv/shiny-server/emdemo/ /srv/shiny-server/shinydebug

# Then run shiny-server
    shiny-server

# Interactive Session
    sudo docker exec -i -t shinydebug bash
